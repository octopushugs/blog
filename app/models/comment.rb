class Comment < ActiveRecord::Base
	has_one :posts
	belongs_to :posts
	validates_presence_of :post_id
	validates_presence_of :body
end
